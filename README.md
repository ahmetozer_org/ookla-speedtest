# Ookla SpeedTest CLI for Docker Container.

You can run speedtest in your docker.

## Command
```bash
docker run -it --rm --network host ahmetozer/ookla-speedtest
```
