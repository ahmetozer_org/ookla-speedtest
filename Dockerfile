FROM ubuntu
RUN apt update ;\
apt install gnupg1 apt-transport-https dirmngr ca-certificates -y; \
export INSTALL_KEY=379CE192D401AB61 ; \
export $(cat /etc/lsb-release | grep 'DISTRIB_CODENAME') ; \
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys $INSTALL_KEY ; \
echo "deb https://ookla.bintray.com/debian ${DISTRIB_CODENAME} main" > /etc/apt/sources.list.d/speedtest.list ; \
apt update ; \
apt install speedtest 
